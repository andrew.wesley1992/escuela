package com.everis.escuela.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSaveRequestDto {
	private String name;
	private String description;
	private Double price;

}
